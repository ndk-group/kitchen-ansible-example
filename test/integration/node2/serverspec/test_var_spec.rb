require 'spec_helper'

describe file('/tmp/test2.txt') do
  it{ should_not exist }
end

describe 'failing test' do
  it 'should fail' do
    expect(file('/tmp/test2.txt')).to exist
  end
end
