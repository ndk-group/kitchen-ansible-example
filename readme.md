# general info #
This project is a simple kitchen-ansible(https://github.com/neillturner/kitchen-ansible) template that displays how to register and reuse hostvars between roles. Also it provides simple example of testing target VMs with serverspec (http://serverspec.org).

# setup #

In order to proceed you'll need latest VirtualBox, Vagrant and ruby installed (rvm is recommended as ruby installer).

* download and install latest Vagrant from here: https://www.vagrantup.com/downloads.html
* install virtualbox, could be downloaded from here: https://www.virtualbox.org
* install rvm (https://rvm.io)
* open project directory and run
  * `gem install bundler` which will setup ruby package manager (http://bundler.io)
  * `bundle` that should download and install all gems (ruby libraries) required to run the example (all gems are listed in `Gemfile` file)

# operations #

* run `kitchen converge` that should setup two nodes using ansible playbook
* run `kitchen verify` that should execute serverspec tests defined in `test/integration/node2/serverspec/test_var_spec.rb`
* as a result you should get one __failing__ test and one __passing__ test
